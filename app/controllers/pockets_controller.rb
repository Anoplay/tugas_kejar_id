class PocketsController < ApplicationController
  def index
    @pockets = Pocket.all
  end

  def show
    id = params[:id]
    @pocket = Pocket.find(id)
  end

  def edit
    id = params[:id]
    @pocket = Pocket.find(id)
  end

  def new
    @pocket = Pocket.new
  end

  def create
    pocket = Pocket.create(pocket_params)
    flash[:notice] = "pocket has been created"
    redirect_to pockets_path
  end

  def update
    id = params[:id]
    pocket = Pocket.find(id)
    pocket.update(pocket_params)
    flash[:notice] = "pocket has been updated"
    redirect_to pocket_path(pocket)
  end

  def destroy
    id = params[:id]
    pocket = Pocket.find(id)
    pocket.destroy()
    flash[:notice] = "pocket has been deleted"
    redirect_to pockets_path
  end
  private
  def pocket_params
    params.require(:pocket).permit(:balance, :student_id, :teacher_id)
  end
end
