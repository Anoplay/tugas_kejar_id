class PaymentsController < ApplicationController
  def index
    @payments = Payment.all
  end

  def show
    id = params[:id]
    @payment = Payment.find(id)
  end

  def edit
    id = params[:id]
    @payment = Payment.find(id)
  end

  def new
    @payment = Payment.new
  end

  def create
    payment = Payment.create(payment_params)
    flash[:notice] = "payment has been created"
    redirect_to payments_path
  end

  def update
    id = params[:id]
    payment = Payment.find(id)
    payment.update(payment_params)
    flash[:notice] = "payment has been updated"
    redirect_to payment_path(payment)
  end

  def destroy
    id = params[:id]
    payment = Payment.find(id)
    payment.destroy()
    flash[:notice] = "payment has been deleted"
    redirect_to payments_path
  end
  private
  def payment_params
    params.require(:payment).permit(:id_transaction, :status, :upload)
  end
end
