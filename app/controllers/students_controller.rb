class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def show
    id = params[:id]
    @student = Student.find(id)
  end

  def edit
    id = params[:id]
    @student = Student.find(id)
  end

  def new
    @student = Student.new
  end

  def create
    student = Student.create(student_params)
    flash[:notice] = "Student has been created"
    redirect_to students_path
  end

  def update
    id = params[:id]
    student = Student.find(id)
    student.update(student_params)
    flash[:notice] = "Student has been updated"
    redirect_to student_path(student)
  end

  def destroy
    id = params[:id]
    student = Student.find(id)
    student.destroy()
    flash[:notice] = "Student has been deleted"
    redirect_to students_path
  end
  private
  def student_params
    params.require(:student).permit(:name, :kelas, :age, :username, :address, :city, :nik)
  end
end
