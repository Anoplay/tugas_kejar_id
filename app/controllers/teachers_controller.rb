class TeachersController < ApplicationController
  def index
    @teachers = Teacher.all
  end

  def show
    id = params[:id]
    @teacher = Teacher.find(id)
  end

  def edit
    id = params[:id]
    @teacher = Teacher.find(id)
  end

  def new
    @teacher = Teacher.new
  end

  def create
    teacher = Teacher.create(teacher_params)
    flash[:notice] = "teacher has been created"
    redirect_to teachers_path
  end

  def update
    id = params[:id]
    teacher = Teacher.find(id)
    teacher.update(teacher_params)
    flash[:notice] = "teacher has been updated"
    redirect_to teacher_path(teacher)
  end

  def destroy
    id = params[:id]
    teacher = Teacher.find(id)
    teacher.destroy()
    flash[:notice] = "teacher has been deleted"
    redirect_to teachers_path
  end
  private
  def teacher_params
    params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
  end
end
