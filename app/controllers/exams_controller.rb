class ExamsController < ApplicationController
  def index
    @exams = Exam.all
  end

  def show
    id = params[:id]
    @exam = Exam.find(id)
  end

  def edit
    id = params[:id]
    @exam = Exam.find(id)
  end

  def new
    @exam = Exam.new
  end

  def create
    exam = Exam.create(exam_params)
    flash[:notice] = "exam has been created"
    redirect_to exams_path
  end

  def update
    id = params[:id]
    exam = Exam.find(id)
    exam.update(exam_params)
    flash[:notice] = "exam has been updated"
    redirect_to exam_path(exam)
  end

  def destroy
    id = params[:id]
    exam = Exam.find(id)
    exam.destroy()
    flash[:notice] = "exam has been deleted"
    redirect_to exams_path
  end
  private
  def exam_params
    params.require(:exam).permit(:title, :mapel, :duration, :nilai, :status, :label, :student_id)
  end
end
