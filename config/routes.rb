Rails.application.routes.draw do
  # get 'pockets/index'
  # get 'pockets/show'
  # get 'pockets/edit'
  # get 'pockets/new'
  # get 'payments/index'
  # get 'payments/show'
  # get 'payments/edit'
  # get 'payments/new'
  # get 'reports/index'
  # get 'reports/show'
  # get 'reports/edit'
  # get 'reports/new'
  # get 'teachers/index'
  # get 'teachers/show'
  # get 'teachers/edit'
  # get 'teachers/new'
  # get 'exams/index'
  # get 'exams/show'
  # get 'exams/edit'
  # get 'exams/new'
  root 'students#index'
  resources :students
  resources :exams
  resources :teachers
  resources :reports
  resources :payments
  resources :pockets
  # get 'students/index'
  # get 'students/show'
  # get 'students/edit'
  # get 'students/new'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
